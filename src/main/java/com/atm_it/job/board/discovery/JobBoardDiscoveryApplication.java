package com.atm_it.job.board.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class JobBoardDiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobBoardDiscoveryApplication.class, args);
    }

}
